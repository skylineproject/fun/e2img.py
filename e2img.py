import os
import time
import re
import sys
import base64
version = 13

# NO RUSSIAN PLEASE
# 'charmap' codec can't encode characters in position 164-166: character maps to <undefined>
# spasibo

if os.name == 'nt':
    e2imgdat   = os.getenv('APPDATA')+"\\skyline\\e2img.dat"
    #e2imgpth   = os.getenv('APPDATA')+"\\skyline\\e2img.pth"
    temppth    = os.getenv('APPDATA')+"\\skyline\\temp.png"
    appdatadir = os.getenv('APPDATA')+"\\skyline"
else:
    e2imgdat   = os.environ['HOME']+"/skyline/e2img.dat"
    #e2imgpth   = os.getenv('APPDATA')+"\\skyline\\e2img.pth"
    temppth    = os.environ['HOME']+"/skyline/temp.png"
    appdatadir = os.environ['HOME']+"/skyline"


try:
    from PIL import Image
except:
    print("No Pillow detected! installing!")
    print("Trying method 1...")
    try:
        os.system("pip install Pillow")
        from PIL import Image
    except:
        try:
            print("Trying method 2...")
            os.system("python3 -m pip install Pillow")
            from PIL import Image
        except:
            print("Trying method 3...")
            os.system("python -m pip install Pillow")
            from PIL import Image

def conf_read():
    with open(e2imgdat, "r") as file:
        config = file.read()
        config = config.split(';')
        file.close()
    return(config)

def conf_write(data):
    with open(e2imgdat, "w") as file:
        data2 = ';'.join(data)
        file.write(data2)
        file.close()

def conf_update(index,value):
    conf = conf_read()
    conf[index] = str(value)
    conf_write(conf)


def patch_dialog():
    try:
        import tkinter
        from tkinter import filedialog
        root = tkinter.Tk()
        root.withdraw()
        dirname = filedialog.askdirectory(parent=root,initialdir="/",title='Please select your GarrysMod directory')
    except:
        dirname = input("Game path: \n Example: D:\\software\\steam\\steamapps\\common\\GarrysMod \n>")

    if os.name == 'nt':
        print(dirname.replace("/","\\\\"))
        return(dirname.replace("/","\\\\"))
    else:
        print(dirname)
        return(dirname)

if not os.path.exists(appdatadir):
    os.makedirs(appdatadir)

def config_restore():
    data = []
    data.append(patch_dialog())
    data.append("0")
    data.append("300")
    data = ';'.join(data)
    f = open(e2imgdat, "w")
    f.write(data)
    f.close()


# ur fukin kode FiveSeven

print("PNG to Wire Screen v"+str(version))

if not os.path.exists(e2imgdat):
    config_restore()

config = conf_read()

try:
    if not sys.argv[1]:
        print(1)
except IndexError:
    print("\n\nSettings:\npress 1 to change resolution\npress 2 to change game dir")
    userinput = input(">")
    if userinput == "1":
        newres = input("Enter new resolution >")
        conf_update(2,newres)
        print("New resolution is %s" % newres)
        time.sleep(5)
    elif userinput == "2":
        conf_update(0,patch_dialog())
        print("Sucess!")
    exit()


game_path = config[0]
if os.name == 'nt':
    if not os.path.isdir(game_path+"\\garrysmod\\data"):
        print("Wrong game location!")
        conf_update(0,patch_dialog())
else:
    if not os.path.isdir(game_path+"/garrysmod/data"):
        print("Wrong game location!")
        conf_update(0,patch_dialog())
starttime = int(time.time())
try:
    resolution = int(config[2])
except:
    config_restore()
    config = conf_read()
    resolution = int(config[2])

#try:
def resize_image():
    size = (resolution,resolution)
    original_image = Image.open(sys.argv[1])
    width, height = original_image.size
    print('The original image size is {wide} wide x {height} '
        'high'.format(wide=width, height=height))

    resized_image = original_image.resize(size)
    width, height = resized_image.size
    print('The resized image size is {wide} wide x {height} '
        'high'.format(wide=width, height=height))
    resized_image.save(temppth)

resize_image()
im = Image.open(temppth, 'r')
rgb_im = im.convert('RGB')
a = []

bytess = bytearray()


for y in range(resolution):
    for x in range(resolution):
        for i in rgb_im.getpixel((x,y)):
            a.append(chr(i))

a = ''.join(a)

if resolution > 255:
    bytess.extend(map(ord, chr(0)))
    bytess.extend(map(ord, chr(int(str(resolution)[0]))))
    bytess.extend(map(ord, chr(int(str(resolution)[1]))))
    bytess.extend(map(ord, chr(int(str(resolution)[2]))))
else:
    resr = chr(resolution)
    bytess.extend(map(ord, resr))

bytess.extend(map(ord, a))

print('Took %s seconds' % round( (time.time()-starttime),3 ))

if not os.path.exists(game_path + "\\garrysmod\\data\\e2files\\"):
    os.makedirs(game_path + "\\garrysmod\\data\\e2files\\")

if os.name == 'nt':
    with open(game_path+"\\garrysmod\\data\\e2files\\"+'sasv2.txt', "wb") as file_out:
        file_out.write(bytess)
        print("Ready! \n Writed in "+game_path+"\\garrysmod\\data\\e2files\\"+'sasv2.txt')
        file_out.close()
else:
    with open(game_path+"/garrysmod/data/e2files/"+'sasv2.txt', "wb") as file_out:
        file_out.write(bytess)
        print("Ready! \n Writed in "+game_path+"/garrysmod/data/e2files/"+'sasv2.txt')
        file_out.close()
#except:
#    print("Error! File read error or FiveSeven is debil blya")
#    time.sleep(2)

if time.time()-float(config[1])>60*10:
    print("autoupdate...")
    conf_update(1,str(time.time()))

    import urllib.request
    try:
        nver = int(urllib.request.urlopen("https://gitlab.com/skylineproject/fun/e2img.py/raw/master/version").read().decode("utf-8"))
        if nver>version:
            print("new version detected! Downloading!")
            upd = urllib.request.urlopen("https://gitlab.com/skylineproject/fun/e2img.py/raw/master/e2img.py").read().decode("utf-8")
            print("Installing...")
            selff = open(__file__,"w")
            selff.write(upd)
            selff.close()
            print("Updated!")
            time.sleep(3)
            did = False
            try:
                e2cc = urllib.request.urlopen("https://gitlab.com/skylineproject/fun/e2img.py/raw/master/e2.txt").read().decode("utf-8")
                # for filename in os.listdir(game_path+"\\garrysmod\\data\\expression2\\"):
                #     if filename.find(".txt")==-1:
                #         continue
                #     f = open(game_path+"\\garrysmod\\data\\expression2\\"+filename, "r")
                #     if f.read().find("name Digiscreen file by Dusty")!=-1:
                #         print("found digiscript: "+filename)
                #         did = True
                #     f.close()
                #     f = open(game_path+"\\garrysmod\\data\\expression2\\"+filename, "w")
                #     f.write(e2cc)
                #     f.close()
                # if did==False:
                if os.name == 'nt':
                    f = open(game_path+"\\garrysmod\\data\\expression2\\e2img.txt", "w")
                else:
                    f = open(game_path+"/garrysmod/data/expression2/e2img.txt", "w")
                f.write(e2cc)
                f.close()
            except:
                print("e2 upd error!")
                time.sleep(1)
    except:
        print("No connection! you loh xd")
