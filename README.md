# e2img.py

* Релизы (качать тут): https://gitlab.com/skylineproject/fun/e2img.py/tags/

* И так! Для начала вам мы установим Python версии 3 или больше.

    Далее берём любую картинку и переносим её на скрипт, он сам изменит её разрешение до 100 на 100 (можете изменить путём запуска скрипта без картинки)   
     
    При первом запуске скрипт спросит вас местоположение игры ( папка \steamapps\common\GarrysMod )
    
    Теперь ставим E2 чип (e2.txt) и подключаем его к Digital Screen, и после этого обновляем его. Всё! Теперь ваша картинка рисуется на Digital Screen!

* По всем вопросам писать `@Shestifled#9993` `@FiveSeven#1233` или `shestifled@gmail.com`

* https://gitlab.com/skylineproject/fun/e2img.py/blob/master/LICENSE
* https://fiveseven.tk/
* https://discord.gg/Hpn8B6J